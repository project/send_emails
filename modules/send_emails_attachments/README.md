# Send Emails Attachments

Allow users to send attachments.

Extends the current `send_emails.mail` service with added functionality, namely:

 - `::resetAttachments()`: reset the file attachments
 - `::addAttachmentByFile(\Drupal\file\FileInterface $file)`: add an attachment by the file entity
 - `::addAttachmentByArrayUnrecommended(array $attachmentDetails)`: add an attachment according to attachment mailer's specifications

## Example

```php
/** @var \Drupal\send_emails_attachments\EmailAttachmentApi */
$sendEmails = \Drupal::service('send_emails.mail.with_attachments');

$filesToAttach = [
    File::load(...),
    File::load(...),
    File::load(...),
];

// Reset the attachments just to be safe...
$sendEmails->resetAttachments();

foreach ($filesToAttach as $file) {
    $sendEmails->addAttachmentByFile($file);
}

$roleToSend = 'site_editors';
$emailTemplate = 'my_email_to_send_with_attachments';
$sendEmails->notifyUsersByRole($roleToSend, $emailTemplate);
```