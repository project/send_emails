<?php

namespace Drupal\send_emails_attachments;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Url;
// For vertification.
use Drupal\user\UserInterface;
use \Drupal\file\FileInterface;

/* Classes used in the __construct */
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountInterface;
use Drupal\send_emails\EmailApi;
use Psr\Container\ContainerInterface;

/**
 * Provides settings for notify user.
 *
 * @package Drupal\EmailApi
 */
class EmailAttachmentApi extends EmailApi {

  /**
   * @see https://www.drupal.org/docs/contributed-modules/mime-mail/for-developers/file-attachments
   * 
   * @var array{filename?: string, filecontent?: string|false, filemime?: string}[]
   */
  private $attachments = [];

  /**
   * Add an attachment by a file entity
   */
  public function addAttachmentByFile(FileInterface $file): void {
    // TODO: Allow support for multiple mail modules.
    // Currently only support MIME Mail
    // @see https://www.drupal.org/docs/contributed-modules/mime-mail/for-developers/file-attachments
    $newAttachment = [
        'filepath' => $file->getFileUri(),
    ];
    $this->addAttachment($newAttachment);
  }

  /**
   * Reset attachments
   */
  public function resetAttachments(): void {
    $this->setAttachments([]);
  }

  /**
   * Use addAttachmentByFile instead or create a new issue stating the new format you'd like
   * to support: https://www.drupal.org/project/issues/send_emails
   * 
   * @param array{filename?: string, filecontent?: string|false, filemime?: string} $attachment
   */
  public function addAttachmentByArrayUnrecommended(array $attachment): void {
    $this->addAttachment($attachment);
  }

  /**
   * @param array{filename?: string, filecontent?: string|false, filemime?: string}[] $attachments
   */
  public function setAttachments(array $attachments): void {
    $this->attachments = $attachments;
  }

  /**
   * @param array{filename?: string, filecontent?: string|false, filemime?: string} $attachment
   */
  private function addAttachment(array $attachment): void {
    $this->attachments[] = $attachment;
  }

  /**
   * 
   */
  private function getAttachments() {
    return $this->attachments;
  }

  /**
   * Override the function to add attachments. Reset attachments after sending.
   */
  protected function sendTemplate(string $to, string $template, array $data = [], $replyTo = '', $send = TRUE, array $params = []): bool {
    $params['send_emails_attachments']['attachments'] = $this->getAttachments();
    $result = parent::sendTemplate($to, $template, $data, $replyTo, $send, $params);

    // $this->resetAttachments();
    
    return $result;
  }

}
