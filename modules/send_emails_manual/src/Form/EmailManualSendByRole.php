<?php

namespace Drupal\send_emails_manual\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\send_emails\Form\EmailSettings;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
// Dependency Injection.
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\send_emails\EmailApi;

/**
 * EmailManualSendByRole for sending emails manually.
 */
class EmailManualSendByRole extends EmailSettings {
  /**
   * Cached user roles.
   *
   * @var array
   */
  protected $userRolesList = NULL;

  /**
   * An instance of the entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * DateFormatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * CacheRender service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * CurrentRouteMatch service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * EmailApi service.
   *
   * @var \Drupal\send_emails\EmailApi
   */
  protected $emailApi;

  /**
   * An array of configuration names that should be editable.
   *
   * @var array
   */
  protected $editableConfig = [];

  /**
   * The machine name of the user role to send.
   *
   * @var string
   */
  protected $userRole = '';

  /**
   * ConfigFactory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new EmailManualSendByRole.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The date formatter.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   The cache render.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match.
   * @param \Drupal\send_emails\EmailApi $emailApi
   *   The email api.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager, DateFormatter $dateFormatter, CacheBackendInterface $cacheRender, CurrentRouteMatch $current_route_match, EmailApi $emailApi, ModuleHandlerInterface $module_handler, AccountProxyInterface $current_user) {
    $this->emailApi = $emailApi;
    $this->currentRouteMatch = $current_route_match;
    parent::__construct($configFactory, $entityTypeManager, $dateFormatter, $cacheRender, $module_handler, $current_user);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory'),
          $container->get('entity_type.manager'),
          $container->get('date.formatter'),
          $container->get('cache.render'),
          $container->get('current_route_match'),
          $container->get('send_emails.mail'),
          $container->get('module_handler'),
          $container->get('current_user'),
      );
  }

  /**
   * Only show emails that begin with the substring 'email_to_send'.
   *
   * @var array
   */
  protected $emailToSend;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'send_emails_manual_send_by_role';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $email_to_send = NULL) {
    $form = parent::buildForm($form, $form_state);

    // Save the parameter.
    $this->emailToSend = $email_to_send;

    // Get user role from query.
    $this->userRole = (string) $this->currentRouteMatch->getRawParameter('role');

    // If the specified email does not exist, throw an error.
    $specifiedEmail = $form['emails'][$this->emailToSend] ?? FALSE;
    if (!$specifiedEmail) {
      $this->logger('send_emails_manual')->notice(
        "Tried accessing Manual Send Emails with invalid template @template (and user role @userrole)",
        ['@template' => $email_to_send, '@userrole' => $this->userRole]);
      throw new NotFoundHttpException();
    }

    // Check if user role exists.
    $userRoleName = 'User';
    if (!empty($this->userRole)) {
      $userRolesList = $this->getUserRoles();
      $userRoleName = $userRolesList[$this->userRole] ?? FALSE;
      if (!$userRoleName) {
        $this->logger('send_emails_manual')->notice(
          "Tried accessing Manual Send Emails with invalid user role @userrole (and template @template)",
          ['@template' => $email_to_send, '@userrole' => $this->userRole]);
        throw new NotFoundHttpException();
      }

      // Set title.
      $form['#title'] = $this->t(
            'Send %type Email to the %role role',
            [
              '%type' => ucwords(str_replace('_', ' ', $this->emailToSend)),
              '%role' => $userRoleName,
            ]
        );
    }
    else {
      // Set Title.
      $form['#title'] = $this->t(
            'Send %type Email to a User Role',
            [
              '%type' => ucwords(str_replace('_', ' ', $this->emailToSend)),
            ]
        );
    }

    // Adjust current form.
    unset($form['emails_definitions']);

    // Remove "URL to Redirect to".
    $specifiedEmail['destination']['#access'] = FALSE;

    // Remove all emails except the specified one.
    unset($form['emails']);
    $form['emails'][$this->emailToSend] = $specifiedEmail;

    // Choose the role.
    if (empty($this->userRole)) {
      $form['role_to_send_email'] = [
        '#title' => 'Which Roles to Send Email to',
        '#type' => 'checkboxes',
        '#options' => $this->getUserRoles(),
        '#required' => TRUE,
      ];
    }

    $form['actions']['submit']['#value'] = $this->t('Send Emails Now');
    // @todo Make constant.
    $form['actions']['submit']['#name'] = 'submit__send_now';

    // Create Draft Button.
    $form['#prefix'] = '<div id="send-emails-manual-send-by-role-wrapper">';
    $form['#suffix'] = '</div>';
    $form['actions']['save_draft'] = [
      '#type' => 'button',
      '#value' => $this->t('Save Draft'),
    // @todo Make constant.
      '#name' => 'submit__save_draft',
      '#limit_validation_errors' => [],
      '#button_type' => 'primary',
    // Move before submit button.
      '#weight' => -10,
      '#ajax' => [
        'callback' => [$this, 'ajaxSubmitForm'],
        'wrapper' => 'send-emails-manual-send-by-role-wrapper',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the email settings (must save before sending to allow processing)
    parent::submitForm($form, $form_state);

    $buttonClicked = $form_state->getTriggeringElement()['#name'] ?? '';
    switch ($buttonClicked) {
      case 'submit__send_now':

        // Support User Role by query.
        // @todo make this condition a helper function.
        if (!empty($this->userRole)) {
          $notificationRoles = [(string) $this->userRole];
        }
        else {
          $notificationRoles = array_filter($form_state->getValue('role_to_send_email'));
        }

        // Email template to send.
        $emailTemplate = $this->emailToSend;

        // Get the form field values for sending emails.
        $destination = $form_state->getValue([
          $this->emailToSend,
          'destination',
        ]);
        $replyTo = $form_state->getValue([$this->emailToSend, 'replyTo']);

        if (!empty($notificationRoles)) {
          $userRolesList = $this->getUserRoles();
          $userRolesSuccess = [];
          $userRolesFailed = [];

          try {
            foreach ($notificationRoles as $notificationRole) {
              $result = $this->emailApi->notifyUsersByRole($notificationRole, $emailTemplate, $destination, $replyTo);

              if ($result) {
                $userRolesSuccess[$notificationRole] = $userRolesList[$notificationRole];
              }
              else {
                $userRolesFailed[$notificationRole] = $userRolesList[$notificationRole];
              }
            }
          }
          catch (\Exception $e) {
            $this->messenger()->addError(
                  $this->t(
                      "The emails failed to send. Please contact the administrator. \n Debugging info: %json",
                      ['%json' => htmlspecialchars(json_encode($e->getMessage()))],
                  )
              );
          }

          if (!empty($userRolesSuccess)) {
            $this->messenger()->addMessage(
                  $this->formatPlural(
                      count($userRolesSuccess),
                      'Users with the role %roles have been notified.',
                      'Users with any of the roles %roles have been notified.',
                      ['%roles' => implode(', ', $userRolesSuccess)]
                  )
              );
          }

          if (!empty($userRolesFailed)) {
            $this->messenger()->addError(
                  $this->formatPlural(
                      count($userRolesFailed),
                      'At least one user with the role %roles could not be notified. Please contact the administrator.',
                      'At least one user with the roles %roles could not be notified. Please contact the administrator.',
                      ['%roles' => implode(', ', $userRolesFailed)]
                  )
              );
          }
        }
        else {
          $this->messenger()->addError(
                $this->t(
                    'No roles are selected'
                )
            );
        }
        break;

      case 'submit__save_draft':
        // Parent submission already handles this case.
        break;

      default:
        $this->messenger()->addWarning(
              $this->t(
                  'There was an problem handling processing the form.'
              )
          );
        break;
    }
  }

  /**
   * Submit the form using ajax.
   */
  public function ajaxSubmitForm(array &$form, FormStateInterface $form_state) {
    $this->submitForm($form, $form_state);
    return $form;
  }

  /**
   * Return an array of user roles keyed by machine name.
   */
  public function getUserRoles() {
    if (is_null($this->userRolesList)) {
      $userRoleStorage = $this->entityTypeManager->getStorage('user_role');

      $userList = array_map(
            function ($userRole) {
                return ucfirst($userRole->label());
            }, $userRoleStorage->loadMultiple()
        );

      // Remove administrator & anonymous users.
      unset($userList['administrator']);
      unset($userList['anonymous']);
      unset($userList['authenticated']);

      $this->userRolesList = $userList;
    }

    return $this->userRolesList;
  }

}
