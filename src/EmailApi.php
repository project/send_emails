<?php declare(strict_types = 1);

namespace Drupal\send_emails;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Url;
// For vertification.
use Drupal\user\UserInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Render\MarkupInterface;

/* Classes used in the __construct */
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Psr\Container\ContainerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides settings for notify user.
 *
 * @package Drupal\EmailApi
 */
class EmailApi {

  use StringTranslationTrait;

  /**
   * The AutoLoginUrlCreate Service
   * TODO: How can we declare the type if we don't know it exists?
   * @var Drupal\auto_login_url\AutoLoginUrlCreate
   */
  private $aluCreateService;

  /**
   * Logger for this service
   */
  protected readonly \Psr\Log\LoggerInterface $logger;

  /**
   * The TWIG template to override the email body with
   */
  protected string $bodyTemplateOverride;

  /**
   * The TWIG template to override the email subject with
   */
  protected string $subjectTemplateOverride;

  /**
   * Class constructor.
   */
  public function __construct(
    protected EntityTypeManager $entityTypeManager,
    protected MailManager $mailManager,
    protected Connection $connection,
    protected DateFormatter $dateFormatter,
    protected TimeInterface $timeService,
    protected MessengerInterface $messenger,
    protected AccountInterface $currentUser,
    protected RendererInterface $renderer,
    protected UrlGeneratorInterface $urlGenerator,
    protected ModuleHandlerInterface $moduleHandler,
    ConfigFactory $config,
    LoggerChannelFactoryInterface $loggerFactory,
  ) {
    // $this->logger = $logger;
    $this->logger = $loggerFactory->get('Send Emails');

    // For Email and Site Name.
    $this->siteConfig = $config->get('system.site');

    $this->emailConfig = $config->get('send_emails.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.mail'),
      $container->get('database'),
      $container->get('date.formatter'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('current_user'),
      $container->get('renderer'),
      $container->get('url_generator'),
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
    );
  }

  /**
   * Notify active users with a role.
   *
   * @param string $roleToNotify
   *   String the machine name of the role users must have to receive an email.
   * @param string $template
   *   String the template to send emails to.
   * @param string $destination
   *   The path to redirect to after logging in.
   * @param string $replyTo
   *   The email to send emails when replying.
   */
  public function notifyUsersByRole(string $roleToNotify, string $template, string $destination = '', $replyTo = '', $data = [], array $params = []): bool {

    $users = $this->entityTypeManager
      ->getStorage('user')
      ->loadByProperties(
              [
                'status' => 1,
                'roles' => $roleToNotify,
              ]
          );

    return $this->notifyByUsers($users, $template, $destination, $replyTo, $data, $params);
  }

  /**
   * Notify Users by providing an array of users.
   *
   * @param array $users
   *   UserInterface[] the users to send emails to.
   * @param string $template
   *   String the template to send emails to.
   * @param string $destination
   *   The path to redirect to after logging in.
   * @param string $replyTo
   *   The email to send emails when replying.
   */
  public function notifyByUsers(array $users, string $template, string $destination = '', $replyTo = '', $data = [], array $params = []): bool {
    $result = TRUE;

    foreach ($users as $user) {
      $emailResult = $this->notifyUser($user, $template, $destination, $replyTo, $data, '', $params);

      if (!$emailResult) {
        $result = FALSE;
      }
    }

    return $result;
  }

  /**
   * Notify Users by providing an array of users.
   *
   * @param array $emailDetails
   *   The information to send emails to['name' => '', 'email' => ''].
   * @param string $template
   *   String the template to send emails to.
   * @param string $destination
   *   The path to redirect to after logging in.
   * @param string $replyTo
   *   The email to send emails when replying.
   * @param array $data
   *   Data that can be accessed in the TWIG template.
   */
  public function notifyByDetails(array $emailDetails, string $template, string $destination = '', $replyTo = '', array $data = [], array $params = []): bool {
    $result = TRUE;

    $dataErrors = [];
    $emailErrors = [];

    foreach ($emailDetails as $emailDetail) {
      if (!isset($emailDetail['name'], $emailDetail['email'])) {
        $result = FALSE;
        $dataErrors[] = $emailDetail;
      }
      else {
        $emailResult = $this->notifyEmail($emailDetail, $template, $destination, $replyTo, $data, $params);

        if (!$emailResult) {
          $result = FALSE;
          $emailErrors[] = $emailDetail['name'] . ' <' . $emailDetail['email'] . '>';
        }
      }

    }

    if (!empty($dataErrors)) {
      $this->logger->error(
            $this->t(
                'Malformed data when sending %template: @data',
                ['%template' => $template, '@data' => json_encode($dataErrors)]
            )
        );
    }

    if (!empty($emailErrors)) {
      $this->messenger->addError(
            $this->t(
                'Unable to notify %count users: %userList',
                [
                  '%count' => count($emailErrors),
                  '%userList' => implode(',', $emailErrors),
                ]
            )
        );
      $this->logger->error(
            $this->t(
                'Failed emails when sending %template: @data',
                ['%template' => $template, '@data' => json_encode($emailErrors)]
            )
        );
    }

    return $result;
  }

  /**
   * Public function to notify a single user by entity or id.
   *
   * @param int $user
   *   UserInterface|int the user entity or user id to send emails to.
   * @param string $template
   *   String the template to send emails to.
   * @param string $destination
   *   The path to redirect to after logging in.
   * @param string $replyTo
   *   The email to send emails when replying.
   * @param array $data
   *   Data that can be accessed in the TWIG template.
   * @param string $toEmail
   *   The email to send to.
   *
   * @return bool
   *   TRUE if the email was sent, FALSE if not.
   */
  public function notifyUser($user, string $template, string $destination = '', string $replyTo = '', array $data = [], string $toEmail = '', array $params = []): bool {

    // Load User.
    if (!($user instanceof UserInterface)) {
      if (is_int($user)) {
        $userStorage = $this->entityTypeManager->getStorage('user');
        $user = $userStorage->load($user);
      }
      else {
        $this->logger->error('User is not loaded properly');
        return FALSE;
      }
    }

    // Generate the destination links
    $data['destination_link'] = $this->getAbsoluteUrlFromUserInput($destination);

    $destinationTrimmed = ltrim($destination, '/');
    $data['auto_login_link'] = $this->createAutoLoginLink($user, $destinationTrimmed);

    // Prepare Body Data.
    $data = [
      'misc' => [
        'userEntity' => $user,
      ],
    ] + $data;

    // Prepare Email.
    $userData = [
      'name' => $user->getDisplayName(),
      'email' => $toEmail === '' ? $user->getEmail() : $toEmail,
    ];

    if (empty($userData['email'])) {
      $this->messenger->addWarning(
            $this->t(
                'Unable to notify <a href="@userLink">%user</a> because the user does not have an email',
                [
                  '%user' => $user->getDisplayName(),
                  '@userLink' => $user->toUrl('edit-form')->toString(),
                ]
            )
        );
      return FALSE;
    }
    else {
      $returnResult = $this->notifyEmail($userData, $template, $destination, $replyTo, $data, $params);
    }

    return $returnResult;
  }

  /**
   * Public function to notify using a name and email.
   *
   * @param array $userData
   *   The information to send emails to ['name' => '', 'email' => ''].
   * @param string $template
   *   String the template to send emails to.
   * @param string $destination
   *   The path to redirect to after logging in.
   * @param string $replyTo
   *   The email to send emails when replying.
   * @param array $data
   *   Data that can be accessed in the TWIG template.
   *
   * @throws \Exception
   *   When $userData is not properly formatted.
   *
   * @return bool|array
   *   Result if the email was sent, FALSE if not.
   */
  public function notifyEmail(array $userData, string $template, string $destination = '', string $replyTo = '', array $data = [], array $params = []): bool {

    if (!isset($userData['name'], $userData['email'])) {
      throw new \Exception('User data does not contain "name" or "email" key.');
    }

    // Set default data.
    if (empty($destination)) {
      $destination = $this->emailConfig->get('emails.' . $template . '.destination') ?? '/';
    }

    if (empty($replyTo)) {
      $replyTo = $this->emailConfig->get('emails.' . $template . '.replyTo') ?? '';
    }

    // Prepare link to destination.
    $destination = $this->getAbsoluteUrlFromUserInput($destination);

    // Prepare Body Data.
    $data['name'] = $userData['name'];
    $data['destination_link'] = $destination;
    $data['misc']['time-raw'] = $this->timeService->getRequestTime();

    // Prepare Email.
    $to = $userData['email'];
    $returnResult = $this->sendTemplate($to, $template, $data, $replyTo, $params);

    return $returnResult;
  }

  /**
   * Private function that returns the headers for the emails.
   *
   * @return bool|array
   *   Email was sent, FALSE if not.
   */
  public function sendSimple(string $to, string $subject, string $body, array $params = []): bool {
    $replyTo = $this->siteConfig->get('mail');
    $message['to'] = $to;
    $message['subject'] = $subject;
    $message['body'][] = $body;
    $message['headers'] = $this->getHeaders($replyTo, $params);
    $message['params'] = [
      'html' => TRUE,
    ];
    $message['send'] = TRUE;

    $mailer = new PhpMail();
    $message = $mailer->format($message);

    return $mailer->mail($message);
  }

  /**
   * Function to send template email through.
   */
  public function sendTemplateEmail(string $to, string $template, array $data = [], $replyTo = NULL, $send = TRUE, array $params = []): bool {

    return $this->sendTemplate($to, $template, $data, $replyTo, $send, $params);
  }

  /**
   * Public function to send emails based on emails.
   *
   * @return bool
   *   TRUE if email template is available,FALSE if template is not available.
   */
  protected function sendTemplate(string $to, string $template, array $data = [], $replyTo = '', $send = TRUE, array $params = []): bool {

    if (empty($this->emailConfig->get('emails.' . $template))) {
      $this->logger->error(
            'Email Template @template does not exist', [
              '@template' => $template,
            ]
        );
      return FALSE;
    }

    // Set reply to site email by default.
    if (empty($replyTo)) {
      $replyTo = $this->siteConfig->get('mail');
    }

    // Add additional data.
    $data['site_name'] = $data['site_name'] ?? $this->siteConfig->get('name');
    $data['site_front'] = $this->getSiteBaseUrl();

    // Format subject & body.
    $subject = $this->getSubjectFromTemplate($template, $data);
    $body = $this->getBodyFromTemplate($template, $data);

    // Set the message.
    $message['to'] = $to;
    $message['subject'] = $subject;
    $message['body'][] = $body;
    $message['headers'] = $this->getHeaders($replyTo, $params);
    $message['params'] = [
      'html' => TRUE,
    ];

    $module = 'send_emails';
    $key = 'send_emails.template.' . $template;
    $to = $to;
    $params['message'] = $message;
    $langcode = $this->currentUser->getPreferredLangcode();
    $send = TRUE;

    $result = $this->mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);

    if (is_array($result) && isset($result['result'])) {
      return $result['result'];
    }
    else {
      return $result;
    }

  }

  /**
   * Allow the body template to be override.
   *
   * @todo Refactor to reduce the number of parameters passed down functions
   */
  public function overrideBodyTemplate(string $bodyTemplateOverride): void {
    $this->bodyTemplateOverride = $bodyTemplateOverride;
  }

  /**
   * Format the Template & allow for overrides.
   */
  private function getBodyFromTemplate(string $template, array $data): MarkupInterface {
    $templateText = '';
    if (!empty($this->bodyTemplateOverride)) {
      $templateText = $this->bodyTemplateOverride;
    }
    else {
      $templateText = $this->emailConfig->get('emails.' . $template . '.body');
    }

    $templateText = '<body style="font-size: 14px; color: #000;">' . $templateText . '</body>';
    $render = [
      '#type' => 'inline_template',
      '#template' => $templateText,
      '#context' => $data,
    ];

    return $this->renderer->renderPlain($render);
  }

  /**
   * Allow the subject template to be override.
   *
   * @todo Refactor to reduce the number of parameters passed down functions
   */
  public function overrideSubjectTemplate(string $subjectTemplateOverride): void {
    $this->subjectTemplateOverride = $subjectTemplateOverride;
  }

  /**
   * Format the Template & allow for overrides.
   */
  private function getSubjectFromTemplate(string $template, array $data): MarkupInterface {
    $templateText = '';
    if (!empty($this->subjectTemplateOverride)) {
      $templateText = $this->subjectTemplateOverride;
    }
    else {
      $templateText = $this->emailConfig->get('emails.' . $template . '.subject');
    }

    $render = [
      '#type' => 'inline_template',
      '#template' => $templateText,
      '#context' => $data,
    ];

    return $this->renderer->renderPlain($render);
  }

  /**
   * Private function that returns the headers for the emails.
   *
   * @return array
   *   The headers for the email.
   */
  private function getHeaders(string $replyTo, array $params): array {
    $paramsHeaders = $params['headers'] ?? [];
    $from = $this->siteConfig->get('mail');
    $headers = $paramsHeaders + [
      'Content-Type' => 'text/html',
      'MIME-Version' => '1.0',
      'Reply-To' => $replyTo,
      'From' => $this->siteConfig->get('name') . ' <' . $from . '>',
    ];

    return $headers;
  }

  /**
   * Get the base url of the website (supports subdirectories)
   * TODO: inject these services
   */
  private function getSiteBaseUrl(): string {
    return $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]);
  }

  /**
   * Get absolute path to destination based on user input
   */
  private function getAbsoluteUrlFromUserInput(string $destinationFromUser): string {
    $destination = $destinationFromUser;

    // Prepare link to destination.
    if (substr($destination, 0, 1) === '/') {
      $destinationTrimmed = $this->removeSubdomainFromUrl($destination);
      $destination = $this->getSiteBaseUrl() . $destinationTrimmed;
    }

    return $destination;
  }

  /**
   * Remove subdomain from generated URLS (as a workaround for send_emails for now)
   * (Also, removes the first slash)
   */
  private function removeSubdomainFromUrl(string $destination): string {
    $front = $this->urlGenerator->generateFromRoute('<front>');
    $result = null;
    if (str_starts_with($destination, $front)) {
      $result = substr($destination, strlen($front));
    }
    else {
      $result = ltrim($destination, '/');
    }
    return $result;
  }

  /**
   * Create an auto login link
   * @param UserInterface $user
   *   The user to create the auto login url login for
   * @param string $localDestination
   *   Must not start with a `/` nor include the subdomain
   * 
   * TODO: Inject these services
   */
  private function createAutoLoginLink(UserInterface $user, string $localDestination): string {
    // Auto login link to the destination page
    if ($this->moduleHandler->moduleExists('auto_login_url')) {
      // TODO: Allow us to mock the alu service
      $isAbsolutePath = TRUE;
      $autoLoginLink = $this->autoLoginUrlCreate()->create($user->id(), $localDestination, $isAbsolutePath);

      return $autoLoginLink;
    }

    return null;
  }

  /**
   * Get the auto login link url service
   */
  public function setAutoLoginUrlCreate($aluCreateService) {
    $this->aluCreateService = $aluCreateService;
  }

  /**
   * Get the auto login link url service
   */
  public function autoLoginUrlCreate() {
    if (empty($this->aluCreateService)) {
      $this->aluCreateService = \Drupal::service('auto_login_url.create');
    };

    return $this->aluCreateService;
  }

}
