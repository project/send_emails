<?php

namespace Drupal\send_emails\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
// Dependency Injection.
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * EmailSettings is the form for the email settings.
 */
class EmailSettings extends ConfigFormBase {
  /**
   * An instance of the entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * An instance of the date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * An instance of the cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * An array of configuration names that should be editable.
   *
   * @var array
   */
  protected $editableConfig = [];

  /**
   * An instance of the Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * An instance of the current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory,
  EntityTypeManagerInterface $entityTypeManager,
  DateFormatter $dateFormatter,
  CacheBackendInterface $cacheRender,
  ModuleHandlerInterface $moduleHandler,
  AccountProxyInterface $currentUser) {
    $this->entityTypeManager = $entityTypeManager;
    $this->dateFormatter = $dateFormatter;
    $this->cacheRender = $cacheRender;

    $this->formConfig = 'send_emails.settings';
    $this->editableConfig[] = $this->formConfig;
    $this->moduleHandler = $moduleHandler;
    $this->currentUser = $currentUser;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory'),
          $container->get('entity_type.manager'),
          $container->get('date.formatter'),
          $container->get('cache.render'),
          $container->get('module_handler'),
          $container->get('current_user')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return $this->editableConfig;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'send_emails_email_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      '__emails_definitions' => [
      ['sample', 'This is sample email.
              Add a new one below in "Email Definitions"',
      ],
      ],
      '__emails_definitions_raw' =>
      'sample|This is sample email. Add a new one below in "Email Definitions"',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $type = NULL) {

    $config = $this->config($this->formConfig);

    // User emails.
    $emailDefinitions = $config->get('__emails_definitions') ?? [];

    foreach ($emailDefinitions as $definition) {

      $definitionName = $definition[0];
      $definitionDescription = $definition[1];

      $emailConfig = $config->get('emails.' . $definitionName);

      $form['emails'][$definitionName] = [
        '#type' => 'details',
        '#title' => $this->t('@name', ['@name' => str_replace('_', ' ', $definitionName)]),
        '#description' => $this->t('@description', ['@description' => $definitionDescription]),
        '#open' => TRUE,
        '#tree' => TRUE,
      ];

      $form['emails'][$definitionName]['subject'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Subject'),
        '#default_value' => $emailConfig['subject'] ?? 'Sample Subject | {{ site_name }}',
      ];

      $form['emails'][$definitionName]['replyTo'] = [
        '#type' => 'email',
        '#title' => $this->t('Reply To Email'),
        '#default_value' => $emailConfig['replyTo'] ?? '',
      ];

      $twigVariables = [
        'name (e.g. username)',
        'time',
        'site_name',
        'site_front',
        'misc.time-raw',
        'misc.userEntity',
        'destination_link',
      ];

      if ($this->moduleHandler->moduleExists('auto_login_url')) {
        $twigVariables[] = 'auto_login_link';

        $defaultBody =
              "<p>Hi {{ name }},</p>" .
  
              "<p>You are receiving this email because a page has been added or updated on {{ site_name }}.</p>" .
  
              "<p>To view this page, please click on the following auto-login link (valid for XX hours):<br/>
              {{auto_login_link}}</p>" .
              "<br>
  
              <p>Thank you,</p> 
  
              <p>{{ site_name }}</p>";
      }
      else {
        $defaultBody =
              "<p>Hi {{ name }},</p>" .
  
              "<p>You are receiving this email because a page has been added or updated on {{ site_name }}.</p>" .
  
              "<p>To view this page, please go to the website: {{ destination_link }} " .
              
              "<br>
  
              <p>Thank you,</p> 
  
              <p>{{ site_name }}</p>";
      }

      $form['emails'][$definitionName]['body'] = [
        '#type' => 'text_format',
        '#title' => $this->t('Body'),
        '#format' => 'full_html',
        '#description' => $this->t(
            '<strong>TWIG Variables: </strong> @variables. Use as <em>{{ variableName }}</em>',
            ['@variables' => implode(', ', $twigVariables)]
        ),
        '#default_value' => $emailConfig['body'] ?? $defaultBody,
      ];

      if (!$this->moduleHandler->moduleExists('smtp')) {
        $form['emails'][$definitionName]['smtpWarning'] = [
          '#type' => 'markup',
          '#markup' => 'Enable & activate the <a href="https://www.drupal.org/project/smtp" ' .
          'target="_blank">SMTP</a> module to send HTML emails.',
        ];
      }

      $form['emails'][$definitionName]['destination'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Url for Auto Login Link'),
        '#default_value' => $emailConfig['destination'] ?? '',
        '#field_prefix' => '/',
        '#description' => $this->t('e.g. node/23'),
      ];
    }

    // Where users can define or remove emails.
    $form['emails_definitions'] = [
      '#type' => 'details',
      '#title' => $this->t('Email Definitions'),
      '#open' => TRUE,
      '#access' => $this->currentUser->hasPermission('create send_emails emails'),
    ];

    $form['emails_definitions']['__emails_definitions'] = [
      '#type' => 'textarea',
      '#title' => 'Definitions',
      '#description' => $this->t(
        'The emails to be used. Enter one value per line, in the format key|description. <br>
        The key is the stored value. The description is optional.'
      ),
      '#default_value' => $config->get('__emails_definitions_raw') ?? '',
    ];

    // Actions.
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Check the email definitions.
    $definitions = [];
    $definitionsRaw = trim($values['__emails_definitions'] ?? '');

    if (!empty($definitionsRaw)) {
      $explodedDefinitions = explode("\n", $definitionsRaw);
      foreach ($explodedDefinitions as $key => $definition) {
        $definitions[$key] = array_map('trim', explode('|', $definition, 2));

        // Check if there's a description?
        // if (count($definitions[$key]) !== 2) {
        // $form_state->setError(
        // $form['emails_definitions']['__emails_definitions'],
        // $this->t('The %definition is missing a "|" character',
        // ['%definition' => $definition])
        // );
        // }.
        if (preg_match('/^[-\w]+$/', $definitions[$key][0]) === FALSE) {
          $form_state->setError(
                $form['emails_definitions']['__emails_definitions'],
                $this->t(
                    'The %key is not formatted properly in %definition',
                    [
                      '%key' => $definitions[$key][0],
                      '%definition' => $definition,
                    ]
                )
            );
        }
      }
    }

    // $build = [
    // '#type' => 'inline_template',
    // '#template' => $template,
    // '#context' => $context,
    // ];
    // Try {
    // \Drupal::service('renderer')->renderPlain($build);
    // }
    // catch (\Exception $exception) {
    // if ($webform_submission->getWebform()->access('update')) {
    // drupal_set_message(t(
    // 'Failed to render computed Twig value due to error "%error"',
    // ['%error' => $exception->getMessage()]), 'error');
    // }
    // };.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();
    $config = $this->config($this->formConfig);

    // Save old definitions (to detect which emails to delete)
    $configEmailDefs = $config->get('__emails_definitions') ?? [];
    $oldDefinitionKeys = array_column($configEmailDefs, 0);

    // This field might be unset by child classes extending this form.
    if (isset($values['__emails_definitions'])) {
      // Only update definitions if user have permission.
      if ($this->currentUser->hasPermission('create send_emails emails')) {
        // Set the old configuations.
        $definitionsRaw = trim($values['__emails_definitions']);
        $config->set('__emails_definitions_raw', $definitionsRaw);

        // Process the definitions.
        $definitions = empty($definitionsRaw) ? [] : explode("\n", $definitionsRaw);
        foreach ($definitions as &$definition) {
          $definition = array_map('trim', explode('|', $definition, 2));
        }
        $config->set('__emails_definitions', $definitions);
      }
    }

    // Set email config.
    foreach ($oldDefinitionKeys as $key) {
      $value = $values[$key] ?? FALSE;

      // Some email definitions might be unset by
      // child classes extending this form.
      if (!$value) {
        continue;
      }

      if (is_array($value) && isset($value['subject'], $value['body'], $value['body']['value'], $value['destination'], $value['replyTo'])) {
        $config->set('emails.' . $key . '.subject', $value['subject']);
        $config->set('emails.' . $key . '.replyTo', $value['replyTo']);

        // Process the destination.
        $processedDestination = ltrim(trim($value['destination']), '/');
        $config->set('emails.' . $key . '.destination', $processedDestination);

        // Fix &nbsp in twig variables.
        $processedBody = preg_replace('/(?:({{)(?:&nbsp;| )+|(?:&nbsp;| )+(}}))/m', '$1 $2', $value['body']['value']);
        $config->set('emails.' . $key . '.body', $processedBody);
      }
      else {
        $this->messenger()->addError(
              $this->t(
                  'Email with %key is not formed properly. Value: @value', [
                    '%key' => $key,
                    '@value' => json_encode($value, JSON_PRETTY_PRINT | JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS),
                  ]
              )
          );
      }
    }
    $config->save();
  }

}
