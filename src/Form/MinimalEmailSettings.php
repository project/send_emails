<?php

namespace Drupal\send_emails\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for minimal email settings.
 */
class MinimalEmailSettings extends EmailSettings {

  /**
   * Only show emails that begin with the "startsWith" substring.
   *
   * Use 'all': to disable.
   *
   * @var string
   */
  protected $startsWith;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'send_emails_minimal_email_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $starts_with = NULL) {
    $form = parent::buildForm($form, $form_state);
    $form['#title'] = $this->t('Edit @type Emails', ['@type' => ucwords(str_replace('_', ' ', $starts_with))]);
    $this->startsWith = $starts_with;

    // User emails.
    foreach ($form['emails'] as $definitionName => &$definition) {
      // Hide the emails that do not start with the specified string.
      if (!empty($this->startsWith) && $this->startsWith !== "all") {
        if (substr($definitionName, 0, strlen($this->startsWith)) !== $this->startsWith) {
          $definition['#access'] = FALSE;
        }
        $definition['replyTo']['#access'] = FALSE;
        $definition['destination']['#access'] = FALSE;
      }
    }
    $form['emails_definitions']['#access'] = FALSE;
    return $form;
  }

}
